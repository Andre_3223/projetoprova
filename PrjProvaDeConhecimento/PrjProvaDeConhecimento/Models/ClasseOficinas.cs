﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrjProvaDeConhecimento.Models
{
    public class ClasseOficinas
    {
        public List<ListaOficinas> ListaOficinas { get; set; }
        public RetornoErro RetornoErro { get; set; }
        public string Token { get; set; }
    }
}