﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrjProvaDeConhecimento.Models
{
    public class ClasseEntradaIndicacao
    {
        public ClasseIndicacao Indicacao { get; set; }
        public string Remetente { get; set; }
        public string[] Copias { get; set; }
    }
}