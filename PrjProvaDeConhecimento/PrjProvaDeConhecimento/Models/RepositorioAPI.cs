﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PrjProvaDeConhecimento.Models
{
    public class RepositorioAPI
    {
        static HttpClient _httpClient;

        public async Task<ClasseOficinas> GetListaDeOficinas(string codAssociacao, string cpfAssociacao)
        {
            string url = "http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Oficina?codigoAssociacao=601&cpfAssociado={1}";
            ClasseOficinas oficinasResult = new ClasseOficinas();
            try
            {
                using (_httpClient = new HttpClient())
                {
                    var result = await _httpClient.GetAsync(url);
                    if (result.IsSuccessStatusCode)
                    {
                        var content = await result.Content.ReadAsStringAsync();
                        oficinasResult = JsonConvert.DeserializeObject<ClasseOficinas>(content);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return oficinasResult;
        }

        public async Task<ViewBagIndicacao> CadastrarIndicacao(ViewBagIndicacao indicacaoAmigo)
        {
            ViewBagIndicacao resposta = new ViewBagIndicacao();
            string url = "http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Indicacao";
            var conteudo = JsonConvert.SerializeObject(indicacaoAmigo);
            var stringc = new StringContent(conteudo, Encoding.UTF8, "application/json");
            using (_httpClient = new HttpClient())
            {
                var response = _httpClient.PostAsync(url, stringc).Result;
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    resposta = JsonConvert.DeserializeObject<ViewBagIndicacao>(content);
                }
            }
            return resposta;
        }
    }
}