﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrjProvaDeConhecimento.Models
{
    public class ViewBagIndicacao
    {
        public ClasseEntradaIndicacao EntradaIndicacao {get; set;}
        public RetornoErro RetornoErro { get; set; }
        public string Sucesso { get; set; }
    }
}