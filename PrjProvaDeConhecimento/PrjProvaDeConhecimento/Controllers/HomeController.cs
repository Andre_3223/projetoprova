﻿using PrjProvaDeConhecimento.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PrjProvaDeConhecimento.Controllers
{
    public class HomeController : Controller
    {
        RepositorioAPI api = new RepositorioAPI();
        public ActionResult Index()
        {
            return View();
        }

        // GET: ListaDeOficinas/Details/5
        public async Task<ActionResult> Details(string cpfAssociacao, string codAssociacao)
        {
            ClasseOficinas oficinas = new ClasseOficinas();
            try
            {
                oficinas = await api.GetListaDeOficinas(codAssociacao, cpfAssociacao);
                Session["CpfUsuario"] = cpfAssociacao;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(oficinas);
        }

        // GET: ListaDeOficinas/Create
        public async Task<ActionResult> Create(string id, string cpfUsuario, string codAssociacao)
        {
            ClasseOficinas oficinas = new ClasseOficinas();
            ListaOficinas oficina = new ListaOficinas();
            var idOficina = Convert.ToInt32(id);
            try
            {
                oficinas = await api.GetListaDeOficinas(codAssociacao, cpfUsuario);
                oficina = oficinas.ListaOficinas.FirstOrDefault(l => l.Id == idOficina);
                Session["CpfUsuario"] = cpfUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(oficina);
        }

        [HttpPost]
        public async Task<ActionResult> Create(ClasseIndicacao viewIdicacao, string Remetente, string id)
        {
            ViewBagIndicacao retorno = new ViewBagIndicacao();
            ViewBagIndicacao classeIndicacao = new ViewBagIndicacao();
            ClasseEntradaIndicacao indicacaoEntrada = new ClasseEntradaIndicacao();
            viewIdicacao.CodigoAssociacao = 601;
            viewIdicacao.DataCriacao = DateTime.Now;
            indicacaoEntrada.Remetente = Remetente;
            indicacaoEntrada.Indicacao = viewIdicacao;
            classeIndicacao.EntradaIndicacao = indicacaoEntrada;
            try
            {
                retorno = await api.CadastrarIndicacao(classeIndicacao);
                ViewBag.Sucesso = retorno.Sucesso;
                ViewBag.Erro = retorno.RetornoErro.retornoErro;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Create", new {@id = id, @cpfUsuario = viewIdicacao.CpfAssociado, @codAssociacao = viewIdicacao.CodigoAssociacao } );
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}